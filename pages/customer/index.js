import { useState, useEffect } from 'react';
import { Table, Tag, Space, PageHeader, Button, Spin } from 'antd';
import Link from 'next/link';
import axios from 'axios';
import { get, isNil } from 'lodash'

export default function CustomerList() {
    
    const columns = [
        {
            title: 'รหัส',
            dataIndex: 'id',
        },
        {
            title: 'CustomerName',
            dataIndex: 'CustomerName',
            key: 'CustomerName',
            render: text => <a>{text}</a>,
        },
        {
            title: 'ContactName',
            dataIndex: 'ContactName',
            key: 'ContactName',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Telephone',
            dataIndex: 'Telephone',
            key: 'Telephone',
            render: text => <a>{text}</a>,
        },
        {
            title: '🎰',
            key: 'action',
            render: (text, { id }) => (
                <Space size="middle">
                    <Link href={`/customer/${id}`}>
                        <a>ดูรายละเอียด</a>
                    </Link>
                </Space>
            ),
        },
    ];

    // ContactName: "K.Sakkapol"
    // CustomerAddress: "11-14 Krungthonburi4 bkk"
    // CustomerName: "K&O system"
    // EmployeeId: ""
    // Telephone: "02042xxxx"
    // UserID: "accko"
    // id: "100001"
    const [data, setData] = useState()

    useEffect(async () => {
        const result = await axios('http://localhost:8080/api/customer');
        setData(get(result, 'data', []));
    }, []);


    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return <>
        <PageHeader
            className="site-page-header"
            title="รายการลูกค้า"
            onBack={() => window.history.back()}
            extra={[
                <Link href={`/customer/add`}>
                    <Button key="1">สร้างลูกค้า</Button>
                </Link>
            ]}
        >
            <Table columns={columns} dataSource={data} />
        </PageHeader>
    </>
}