import { useState, useEffect } from 'react';
import { Descriptions, Tag, Space, PageHeader, Button } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import axios from 'axios';
import moment from 'moment';
import { get, isNil, find, toInteger } from 'lodash'

export default function ContractView() {
    const { id } = useRouter().query
    const [data, setData] = useState()
    const [employees, setEmployees] = useState([])
    useEffect(async () => {
        if (isNil(id)) return 
        const result = await axios(`http://localhost:8080/api/customer/${id}`);
        setData(get(result, 'data'));
        setEmployees(get(await axios('http://localhost:8080/api/employee'), 'data', []));
    }, [id]);

    return <>
        <PageHeader
            className="site-page-header"
            onBack={() => window.history.back()}
            title="View Customer"
        >
            <Descriptions bordered>
                <Descriptions.Item label="รหัสลูกค้า">
                    {data?.id}
                </Descriptions.Item>
                <Descriptions.Item label="ชื่อลูกค้า">
                    {data?.CustomerName}
                </Descriptions.Item>
                <Descriptions.Item label="ที่อยู่ลูกค้า">
                    {data?.CustomerAddress}
                </Descriptions.Item>
                <Descriptions.Item label="ชื่อผู้ติดต่อ">
                    {data?.ContactName}
                </Descriptions.Item>
                <Descriptions.Item label="พนักงาน">
                    {data?.EmployeeId}
                    {find(employees, ['id', toInteger(data?.EmployeeId)])?.EmployeeName || data?.EmployeeId}
                </Descriptions.Item>
                <Descriptions.Item label="เบอร์โทร">
                    {data?.Telephone}
                </Descriptions.Item>
                <Descriptions.Item label="ผู้ใช้งาน">
                    {data?.UserID}
                </Descriptions.Item>
            </Descriptions>
        </PageHeader>
    </>
}