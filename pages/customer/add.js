import { useState, useEffect } from 'react';
import Link from 'next/link'
import { Form, Button, PageHeader, Input, notification, Select } from 'antd'
import { get, eq, map } from 'lodash';
import axios, { post } from 'axios';

const FormItem = Form.Item

export default function TroubleList() {

    const onFinish = async (data) => {
        console.log('🚀 ~ file: add.js ~ line 12 ~ onFinish ~ data', data)
        try {
            const result = await post('http://localhost:8080/api/customer', data);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/customer/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };
    
    const [users, setUsers] = useState([])
    const [employees, setEmployees] = useState([])

    useEffect(async () => {
        setUsers(get(await axios('http://localhost:8080/api/user'), 'data', []));
        setEmployees(get(await axios('http://localhost:8080/api/employee'), 'data', []));
    }, []);

    return <>
        <PageHeader
            className="site-page-header"
            onBack={() => window.history.back()}
            title="สร้างลูกค้า"
        >
            <Form layout="horizontal" onFinish={onFinish}>
                <FormItem
                    name="CustomerName"
                    label="ชื่อลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input />
                </FormItem>
                <FormItem
                    name="CustomerAddress"
                    label="ที่อยู่ลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input />
                </FormItem>
                <FormItem
                    name="ContactName"
                    label="ชื่อผู้ติดต่อ"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input />
                </FormItem>
                <FormItem
                    name="Telephone"
                    label="เบอร์โทร"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input />
                </FormItem>
                <FormItem
                    name="EmployeeId"
                    label="พนักงาน"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select>
                        {map(employees, ({ id, EmployeeName }) => <Select.Option key={id}>{id} - {EmployeeName}</Select.Option>)}
                    </Select>
                </FormItem>
                <FormItem
                    name="UserID"
                    label="ผู้ใช้งาน"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select>
                        {map(users, ({ id }) => <Select.Option key={id}>{id}</Select.Option>)}
                    </Select>
                </FormItem>

                <FormItem
                    style={{ marginTop: 48 }}
                    wrapperCol={{ span: 8, offset: 8 }}
                >
                    <Button size="large" type="primary" htmlType="submit">
                        ยืนยัน
                    </Button>
                    <Button size="large" style={{ marginLeft: 8 }} htmlType="reset">
                        ยกเลิก
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}