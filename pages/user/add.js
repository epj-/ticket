import { Form, Select, Button, PageHeader, Input, notification } from 'antd'

// Custom DatePicker that uses Day.js instead of Moment.js
import DatePicker from '../../components/DatePicker'
import { post } from 'axios';
import { get, eq } from 'lodash'

const FormItem = Form.Item

export default function UserCreateForm() {

    const onFinish = async (data) => {
        try {
            const result = await post('http://localhost:8080/api/user', data);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/user/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };


    return <>
        <PageHeader
            className="site-page-header"
            onBack={() => window.history.back()}
            title="สร้างผู้ใช้งาน"
        >
            <Form layout="horizontal" onFinish={onFinish}>
                <FormItem name="id" label="ผู้ใช้" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Input />
                </FormItem>
                <FormItem name="Password" label="รหัสผ่าน" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Input type="password" />
                </FormItem>
                <FormItem name="Case" label="ประเภท" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Select defaultValue="1" style={{ width: 120 }}>
                        <Select.Option value="0">ผู้ดูแลระบบ</Select.Option>
                        <Select.Option value="1">ลูกค้า</Select.Option>
                        <Select.Option value="2">ฝ่ายประสานงาน</Select.Option>
                        <Select.Option value="3">ฝ่ายบำรุงรักษา</Select.Option>
                        <Select.Option value="3">ฝ่ายบริหาร</Select.Option>
                    </Select>
                </FormItem>
                <FormItem style={{ marginTop: 48 }} wrapperCol={{ span: 8, offset: 8 }}>
                    <Button size="large" type="primary" htmlType="submit">
                        สร้าง
                    </Button>
                    <Button size="large" style={{ marginLeft: 8 }} htmlType="reset">
                        ล้างข้อมูล
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}