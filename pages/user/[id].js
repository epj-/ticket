import { useState, useEffect } from 'react';
import { Descriptions, PageHeader } from 'antd';
import { useRouter } from 'next/router';
import axios from 'axios';
import { get, isNil } from 'lodash'

export const FORMAT_CASE = ['ผู้ดูแลระบบ', 'ลูกค้า', 'ฝ่ายประสานงาน', 'ฝ่ายบำรุงรักษา', 'ฝ่ายบริหาร']

export default function UserView() {
    const { id } = useRouter().query
    const [data, setData] = useState()

    useEffect(async () => {
        if (isNil(id)) return 
        const result = await axios(`http://localhost:8080/api/user/${id}`);
        setData(get(result, 'data'));
    }, [id]);

    return <>
        <PageHeader className="site-page-header" onBack={() => window.history.back()} title="รายละเอียด">
            <Descriptions bordered size="small">
                <Descriptions.Item label="ผู้ใช้งาน">
                    {data?.id}
                </Descriptions.Item>
                <Descriptions.Item label="รูปแบบ">
                    {!!data && FORMAT_CASE[data?.Case]}
                </Descriptions.Item>
            </Descriptions>
        </PageHeader>
    </>
}