import { useState, useEffect } from 'react';
import { Table, Space, PageHeader, Button, Spin } from 'antd';
import Link from 'next/link';
import axios from 'axios';
import { get, isNil } from 'lodash'
import { FORMAT_CASE } from './[id]'

export default function UserList() {
    const columns = [
        {
            title: 'ผู้ใช้งาน',
            dataIndex: 'id',
        },
        {
            title: 'รูปแบบ',
            dataIndex: 'Case',
            key: 'Case',
            render: text => <a>{FORMAT_CASE[text]}</a>,
        },
        {
            title: '🎰',
            key: 'action',
            render: (text, { id }) => (
                <Space size="middle">
                    <Link href={`/user/${id}`}>
                        <a>ดูรายละเอียด</a>
                    </Link>
                </Space>
            ),
        },
    ];

    const [data, setData] = useState()

    useEffect(async () => {
        const result = await axios('http://localhost:8080/api/user');
        setData(get(result, 'data', []));
    }, []);

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return <>
        <PageHeader
            className="site-page-header"
            title="รายการผู้ใช้งาน"
            // subTitle="This is a subtitle"
            onBack={() => window.history.back()}
            extra={[
                <Link href={`/user/add`}>
                    <Button key="1">สร้างผู้ใช้งาน</Button>
                </Link>
            ]}
        >
            <Table columns={columns} dataSource={data} />
        </PageHeader>
    </>
}