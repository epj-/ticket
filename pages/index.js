import { Button } from 'antd'
import { SmileFilled } from '@ant-design/icons'
import Link from 'next/link'

export default function Home() {
  return (
    <div style={{ margin: '100px 200px' }}>
      <div className="text-center mb-5">
        <Link href="#">
          <a className="logo mr-0">
            <SmileFilled size={48} strokeWidth={1} />
          </a>
        </Link>

        <p className="mb-10 mt-10 text-disabled">Welcome to the world !</p>
        <Link href="/issue"><Button style={{ marginTop: '10px'}} shape="round" block >รายการปัญหา</Button></Link>
        <Link href="/contract"><Button style={{ marginTop: '10px'}} shape="round" block >รายการสัญญา</Button></Link>
        <Link href="/customer"><Button style={{ marginTop: '10px'}} shape="round" block >รายการลูกค้า</Button></Link>
        <Link href="/user"><Button style={{ marginTop: '10px'}} shape="round" block >รายการผู้ใช้งาน</Button></Link>
      </div>
      <div>
      </div>
    </div>
  )
}
