import { useState, useEffect } from "react";
import { Descriptions, Tag, Space, PageHeader, Button } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import { get, isNil, find, toInteger } from "lodash";
import moment from "moment";
import "moment/locale/th";
moment.locale("th");

export default function TroubleView() {
  const { id } = useRouter().query;
  const [data, setData] = useState();
  const [customers, setCustomers] = useState([]);
  console.log(
    "🚀 ~ file: [id].js ~ line 15 ~ TroubleView ~ customers",
    customers
  );
  const [employees, setEmployees] = useState([]);
  console.log(
    "🚀 ~ file: [id].js ~ line 16 ~ TroubleView ~ employees",
    employees
  );

  useEffect(async () => {
    if (isNil(id)) return;

    setData(get(await axios(`http://localhost:8080/api/issue/${id}`), "data"));
    setCustomers(
      get(await axios("http://localhost:8080/api/customer"), "data", [])
    );
    setEmployees(
      get(await axios("http://localhost:8080/api/employee"), "data", [])
    );
  }, [id]);

  return (
    <>
      <PageHeader
        className="site-page-header"
        onBack={() => window.history.back()}
        title="View Issue"
      >
        <Descriptions bordered title={`Issue ${data?.id}`} size="small">
          <Descriptions.Item label="รหัสปัญหา">{data?.id}</Descriptions.Item>
          <Descriptions.Item label="รายละเอียดปัญหา" span="2">
            {data?.IssueDetail}
          </Descriptions.Item>
          <Descriptions.Item label="วันที่แจ้งปัญหา">
            {!!data?.IssueDate && moment(data?.IssueDate).format("ll")}
          </Descriptions.Item>
          <Descriptions.Item label="ชื่อลูกค้า" span="2">
            {find(customers, ["id", toInteger(data?.CustomerId)])
              ?.CustomerName || data?.CustomerId}
          </Descriptions.Item>
          <Descriptions.Item label="วันที่แก้ไข">
            {!!data?.FixDate && moment(data?.FixDate).format("ll")}
          </Descriptions.Item>
          <Descriptions.Item label={"ชื่อพนักงาน"} span="2">
            {data?.EmployeeId}
          </Descriptions.Item>
          <Descriptions.Item label="สถานะ">
            <Tag color="geekblue">{data?.IssueStatus}</Tag>
          </Descriptions.Item>
          <Descriptions.Item label={"IssueLevel"} span="2">
            {data?.IssueLevel}
          </Descriptions.Item>
          <Descriptions.Item label="รายละเอียดการแก้ไข" span="3">
            {data?.FixDetail}
          </Descriptions.Item>
        </Descriptions>
      </PageHeader>
    </>
  );
}
