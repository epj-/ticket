import { Form, Button, PageHeader, Input, notification } from 'antd'
import { post } from 'axios';
import { get, eq } from 'lodash';
import moment from 'moment';

const FormItem = Form.Item

export default function TroubleList() {

    const onFinish = async (data) => {
        try {
            data.IssueDate = moment().format('yyyy-MM-DD')
            data.IssueStatus = 'opened'
            const result = await post('http://localhost:8080/api/issue', data);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/issue/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };

    return <>
        <PageHeader
            className="site-page-header"
            onBack={() => window.history.back()}
            title="แจ้งปัญหา"
        >
            <Form layout="horizontal" onFinish={onFinish}>
                <FormItem
                    name="IssueDetail"
                    label="รายละเอียด"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input.TextArea rows={4} />
                </FormItem>
                <FormItem
                    style={{ marginTop: 48 }}
                    wrapperCol={{ span: 8, offset: 8 }}
                >
                    <Button size="large" type="primary" htmlType="submit">
                        สร้าง
                    </Button>
                    <Button size="large" style={{ marginLeft: 8 }} htmlType="reset">
                        ล้างข้อมูล
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}