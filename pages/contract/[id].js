import { useState, useEffect } from 'react';
import { Descriptions, Tag, Space, PageHeader, Button } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import axios from 'axios';
import { get, isNil, find, toInteger } from 'lodash'
import moment from 'moment';
import 'moment/locale/th' 
moment.locale('th')

export default function ContractView() {
    const { id } = useRouter().query
    const [data, setData] = useState()

    const [customers, setCustomers] = useState([])

    useEffect(async () => {
        if (isNil(id)) return 
        const result = await axios(`http://localhost:8080/api/contract/${id}`);
        setData(get(result, 'data'));

        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, [id]);

    return <>
        <PageHeader
            className="site-page-header"
            onBack={() => window.history.back()}
            title="รายละเอียดสัญญา"
        >
            <Descriptions bordered size="small">
                <Descriptions.Item label="รหัสปัญหา">
                    {data?.id}
                </Descriptions.Item>
                <Descriptions.Item label="ลูกค้า" span="2">
                    {find(customers, ['id', toInteger(data?.CustomerId)])?.CustomerName || data?.CustomerId}
                </Descriptions.Item>
                <Descriptions.Item label="วันเริ่มสัญญา">
                    {!!data?.StartDate && moment(data?.StartDate).format('ll')}
                </Descriptions.Item>
                <Descriptions.Item label="วันสิ้นสุดสัญญา">
                    {!!data?.EndDate && moment(data?.EndDate).format('ll')}
                </Descriptions.Item>
            </Descriptions>
        </PageHeader>
    </>
}