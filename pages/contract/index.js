import { useState, useEffect } from 'react';
import { Table, Tag, Space, PageHeader, Button, Spin } from 'antd';
import Link from 'next/link';
import axios from 'axios';
import { get, isNil, find, toInteger } from 'lodash'
import moment from 'moment'
import 'moment/locale/th' 
moment.locale('th')

export default function ContractList() {
    const columns = [
        {
            title: 'รหัส',
            dataIndex: 'id',
        },
        {
            title: 'ลูกค้า',
            dataIndex: 'CustomerId',
            key: 'CustomerId',
            render: CustomerId => <a>{find(customers, ['id', toInteger(CustomerId)])?.CustomerName || CustomerId}</a>,
        },
        {
            title: 'วันเริ่มสัญญา',
            dataIndex: 'StartDate',
            key: 'StartDate',
            render: text => <a>{moment(text).format('ll')}</a>,
        },
        {
            title: 'วันสิ้นสุดสัญญา',
            dataIndex: 'EndDate',
            key: 'EndDate',
            render: text => <a>{moment(text).format('ll')}</a>,
        },
        {
            title: '🎰',
            key: 'action',
            render: (text, { id }) => (
                <Space size="middle">
                    <Link href={`/contract/${id}`}>
                        <a>ดูรายละเอียด</a>
                    </Link>
                </Space>
            ),
        },
    ];

    const [data, setData] = useState()
    const [customers, setCustomers] = useState([])

    useEffect(async () => {
        const result = await axios('http://localhost:8080/api/contract');
        setData(get(result, 'data', []));

        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, []);

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }
    
    return <>
        <PageHeader
            className="site-page-header"
            title="รายการสัญญา"
            onBack={() => window.history.back()}
            extra={[
                <Link href={`/contract/add`}>
                    <Button key="1">สร้างสัญญา</Button>
                </Link>
            ]}
        >
            <Table columns={columns} dataSource={data} />
        </PageHeader>
    </>
}