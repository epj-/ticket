import { useState, useEffect } from 'react';
import Link from 'next/link'
import { Form, Button, PageHeader, Input, notification, Select } from 'antd'
import axios, { post } from 'axios';
import { get, eq, map, set, isNil } from 'lodash';
import moment from 'moment';
import 'moment/locale/th' 
moment.locale('th')

const FormItem = Form.Item

export default function TroubleList() {

    const onFinish = async (data) => {
        try {
            isNil(get(data, 'StartDate')) && set(data, 'StartDate', moment().format('YYYY-MM-DD'))
            isNil(get(data, 'EndDate')) && set(data, 'EndDate', moment().add(1, 'year').format('YYYY-MM-DD'))
            const result = await post('http://localhost:8080/api/contract', data);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/contract/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };


    const [customers, setCustomers] = useState([])

    useEffect(async () => {
        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, []);

    return <>
        <PageHeader
            className="site-page-header"
            onBack={() => window.history.back()}
            title="สร้างสัญญา"
        >
            <Form layout="horizontal" onFinish={onFinish}>
                <FormItem
                    name="CustomerId"
                    label="CustomerId"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select>
                        {map(customers, ({ id, CustomerName }) => <Select.Option key={id}>{CustomerName}</Select.Option>)}
                    </Select>
                </FormItem>
                <FormItem
                    name="StartDate"
                    label="วันเริ่มสัญญา"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input defaultValue={moment().format('YYYY-MM-DD')} />
                </FormItem>
                <FormItem
                    name="EndDate"
                    label="วันสิ้นสุดสัญญา"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input defaultValue={moment().add(1, 'year').format('YYYY-MM-DD')} />
                </FormItem>

                <FormItem
                    style={{ marginTop: 48 }}
                    wrapperCol={{ span: 8, offset: 8 }}
                >
                    <Button size="large" type="primary" htmlType="submit">
                        ยืนยัน
                    </Button>
                    <Button size="large" style={{ marginLeft: 8 }} htmlType="reset">
                        ยกเลิก
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}